ALTER TABLE catalog
ADD FULLTEXT(title,author);



SELECT
title,author
FROM
catalog
WHERE MATCH(title,author)
AGAINST('king carrie' IN NATURAL LANGUAGE MODE);


SELECT
title,author
FROM
catalog
WHERE MATCH(title,author)
AGAINST('king -carrie' IN BOOLEAN MODE);

USE bookstore;

ALTER TABLE book
ADD FULLTEXT(title);

ALTER TABLE author
ADD FULLTEXT(name);

SELECT
book.title,
author.name
FROM
book
JOIN author using(author_id)
WHERE MATCH(book.title)
AGAINST('king carrie' IN NATURAL LANGUAGE MODE)
OR
 MATCH(author.name)
AGAINST('king carrie' IN NATURAL LANGUAGE MODE);


SELECT
title,author
FROM
book_list
WHERE MATCH(title,author)
AGAINST('king carrie' IN NATURAL LANGUAGE MODE);