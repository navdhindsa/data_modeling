<?php
$dbh=new PDO('sqlite:day1.sqlite3');


$query = 'SELECT * FROM users';
$stmt=$dbh->query($query);
$results=$stmt->fetchAll(PDO::FETCH_ASSOC);
?><!DOCTYPE>
<html>
<head>
  <style>
    table{
      border: 1px solid #cfcfcf;
      border-collapse: collapse;
    }
    td,th{
      padding: 6px;
      border: 1px solid #cfcfcf;
    }
    th{
      background: #000;
      color: #fff;
    }
  
  
  </style>
  
  
  </head>
  <body>
  
    <table>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Email</th>
      <th>Phone</th>
      
      </tr>
      <?php foreach($results as $row):?>
      <tr>
        <td><?=$row['id']?></td>
        <td><?=$row['name']?></td>
        <td><?=$row['email']?></td>
        <td><?=$row['phone']?></td>
      </tr>
      <?php endforeach; ?>
    </table>
    
    <br><br>
    <table>
      
      <?php foreach($results as $row):?>
      <tr>
      <?php foreach($row as $key=>$value):?>
      <td><?=$value;?></td>
      <?php endforeach; ?>
      </tr>
      <?php endforeach; ?>
    </table>
  
  </body>
</html>
