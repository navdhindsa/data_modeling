commit e9a372b869f36484574e59fd043efe988436218f
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Sat Jul 14 08:28:25 2018 -0500

    changes to logical model

commit 2db6649e4334304a98acbabfb84cf8f01c843bd0
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Sat Jul 14 08:17:35 2018 -0500

    final commit

commit b3bcee0d0d139202e15c5cbc10cbf994f82edd2f
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Sat Jul 14 08:16:14 2018 -0500

    entities added

commit e66ec6827ea0be0ff34048a36f0bd2ae87f5a766
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Sat Jul 14 08:12:24 2018 -0500

    conceptual model pdf

commit a76412efae128c269caacb1931011073518bce64
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Sat Jul 14 08:11:41 2018 -0500

    conceptual model

commit 4587135f1f139bedb78a9e9cbc06ab3212b71078
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 21:24:50 2018 -0500

    Adding more entities

commit 1884d116892cd556116d2050e7182dfd1179d6d4
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 21:09:57 2018 -0500

    Logical Data model formatting

commit 33feefe4c27e5e6809f0db16a4edc91543a55166
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 21:03:46 2018 -0500

    Logical Data model drawing pdf

commit c45172f9311feb72c831d1baadc0b5ce6e19eb6d
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 21:03:04 2018 -0500

    Logical Data model drawing

commit e64b6c341eea3f1f4553aa30482323eebecb0736
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 20:00:17 2018 -0500

    entities added

commit e2c916782503cf453650725a4f83370877568b26
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 19:58:01 2018 -0500

    entities listed

commit 847c117e1fa38135dc81b83e648514bf0669e57a
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 19:38:11 2018 -0500

    start of assignment

commit 8efab5b9883328ecfbb8ab98fbd8f63823e0ffde
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 15:08:19 2018 -0500

    day 3 queries

commit 39bb4bae3fc7fb5a197bf82d525613adaaeb9d07
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Fri Jul 13 10:51:32 2018 -0500

    day 3 saves

commit e6451ec2a0b9223eab8edebe5874103d24ebf821
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Tue Jul 10 18:43:28 2018 -0500

    sql queries

commit 5ad3d8d86c678a97dfd7df3d25ced818cdf8e533
Author: Navdeep <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Tue Jul 10 18:41:49 2018 -0500

    day 1 work in class

commit 48982e8c95ed7b61f41debf4e6e8108c95e26030
Author: Navdeep Dhindsa <dhindsa-n@webmail.uwinnipeg.ca>
Date:   Tue Jul 10 14:08:39 2018 +0000

    README.md created online with Bitbucket
