CREATE TABLE users (
id INTEGER NOT NULL PRIMARY KEY, 
name VARCHAR, 
email VARCHAR, 
phone VARCHAR, 
password VARCHAR, 
age INTEGER, 
active BOOLEAN);
  
INSERT INTO users (
name, 
email, 
phone, 
password, 
age, 
active) 
VALUES (
"Tom Jones",
"tom@example.com",
"204-222-1234", 
"mypass", 
23, 
1);

INSERT INTO `users` (
`name`,
  `email`,
  `age`,
  `password`,
  `phone`,
  `active`) 
  VALUES (
    "Cameron U. Farmer",
    "quis@pedemalesuada.co.uk",
    18,"KCA78IXQ6CW",
    "1-732-407-4819",
    0);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Candace C. Barry","nisi.nibh.lacinia@nibhPhasellusnulla.com",28,"YLU21ERW4VQ","1-257-683-3321",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Curran Greene","sit.amet@Donecconsectetuer.ca",35,"MRG15XIY8DE","1-735-800-8360",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Hu Mcintyre","dolor.sit@ut.ca",19,"DIG45UWG4ID","1-943-247-4731",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Germaine Y. Cohen","malesuada.fames.ac@penatibuset.edu",40,"JUU86DUF8MU","1-701-374-6683",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Inez Bray","risus@feugiat.org",47,"FTA84TDE3PK","1-851-192-8088",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Madeson H. Harvey","a.feugiat.tellus@Namporttitor.edu",32,"KAG09AHE7II","1-994-149-1062",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Quintessa U. Joyner","mi.eleifend.egestas@parturient.edu",26,"KJT39PWW5KA","1-229-490-3312",1);

INSERT INTO `users` (`name`,`email`,`age`,`password`,`phone`,`active`) VALUES ("Lev Stanton","facilisi.Sed.neque@imperdietnecleo.co.uk",31,"AFN13VQT4YA","1-638-740-0304",1);
