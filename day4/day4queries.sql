SELECT 
book.title,
author.name AS author,
publisher.name AS publisher
FROM
book
JOIN 
author USING (author_id)
JOIN
publisher USING (publisher_id);


SELECT 
book.title,
author.name AS author,
publisher.name AS publisher,
genre.name AS genre,
book.price
FROM
book
JOIN 
author USING (author_id)
JOIN
publisher USING (publisher_id)
JOIN
genre USING (genre_id)
WHERE
author.name LIKE '%king'
;

SELECT 
book.title,
author.name AS author,
author.country AS country

FROM
book
JOIN 
author USING (author_id)
WHERE
author.country IN ('USA','Canada') 
ORDER BY country 
;

SELECT 
book.title,
author.name AS author,
author.country AS country
FROM
book
JOIN 
author USING (author_id)
WHERE
author.country  NOT IN ('USA','Canada') 
ORDER BY country 
;


SELECT 
book.title,
author.name AS author,
publisher.name AS publisher,
book.price
FROM
book
JOIN 
author USING (author_id)
JOIN
publisher USING (publisher_id)

WHERE
price<18
;
SELECT 
book.title,
author.name AS author,
publisher.name AS publisher,
book.price
FROM
book
JOIN 
author USING (author_id)
JOIN
publisher USING (publisher_id)
WHERE
price BETWEEN (10,25)
;

INSERT into author
VALUES (15, 'Diego','EL Salvador');
 
 
INSERT INTO publisher
VALUES (22,'STEVE','Wpg','2044970042');

INSERT INTO book
(
book_id,
title,
year_published,
  num_pages,
  in_print,
  price,

  publisher_id,
  format_id,
  genre_id
)
VALUES(
121,
  "title1",
  1425,
  152,
  0,
  25.50,
  "22",
  
  1,
  1
);

INSERT INTO book
(
book_id,
title,
year_published,
  num_pages,
  in_print,
  price,
  author_id,
  
  format_id,
  genre_id
)
VALUES(
122,
  "title2",
  1825,
  1522,
  0,
  24.50,
  "15",

  1,
  1
);

SELECT
book.book_id,
book.title,
author.author_id,
author.name AS author
FROM 
book
LEFT JOIN author USING (author_id);

SELECT
book.book_id,
book.title,
author.author_id,
author.name AS author
FROM 
book
RIGHT JOIN author USING (author_id);

select * 
FROM book
LEFT JOIN genre USING (genre_id);


SELECT
book.book_id,
book.title,
author.author_id,
author.name AS author
FROM 
book
RIGHT JOIN author USING (author_id)
WHERE title IS null;

SELECT
book.book_id,
book.title,
genre.genre_id,
genre.name AS genre
FROM 
book
LEFT JOIN genre USING (genre_id)
WHERE genre_id IS null;

UPDATE book SET genre_id = null WHERE book_id=9;

SELECT
book_id,
book.title,
genre.genre_id,
genre.name AS genre
FROM book,genre;

 select
 book_id,
 title,price as cost, 
 format(price*1.4,2) as cust_price 
 from book;