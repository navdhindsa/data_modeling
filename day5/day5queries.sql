USE bookstore;

SELECT 
DISTINCT author.name AS author
FROM
book
JOIN
author USING(author_id);

Select 
book.title,
book.price,
author.name AS author,
publisher.name AS publisher
FROM 
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
LIMIT 5;

REPLACE
INTO
book
(book_id, 
 title)
 VALUES
 (3,'A New Book');
 
INSERT INTO book
(book_id)
VALUES(12)
ON DUPLICATE KEY UPDATE
price=13.99;

SELECT
MIN(price) AS 'Minimum Price',
MAX(price) AS 'Maximum Price',
AVG(price) AS 'Average Price'
FROM 
book;

SELECT 
publisher.name,
COUNT(book.book_id) AS 'num_books'
FROM
book
JOIN publisher USING(publisher_id)
GROUP BY publisher.name;


SELECT 
publisher.name,
author.name,
COUNT(book.book_id) AS 'num_books'
FROM
book
JOIN publisher USING(publisher_id)
JOIN author USING(author_id)
GROUP BY publisher.name,author.name;


SELECT
publisher.name,
MIN(price) AS 'Minimum Price',
MAX(price) AS 'Maximum Price',
AVG(price) AS 'Average Price'
FROM 
book
JOIN publisher USING(publisher_id)
GROUP BY publisher.name
;


SELECT 
genre.name,
COUNT(book.book_id) AS 'num_books'
FROM
book
JOIN genre USING(genre_id)
GROUP BY genre.name;

SELECT 
publisher.name,
COUNT(book.book_id) AS 'num_books'
FROM
book
JOIN publisher USING(publisher_id)
GROUP BY publisher.name
HAVING num_books>2;

SELECT 
publisher.name as publisher,
format.name as format,
COUNT(book.book_id) AS 'num_books'
FROM
book
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
GROUP BY publisher.name,format.name;

CREATE VIEW book_list
AS
SELECT 
book.title,
book.price,
author.name AS author,
publisher.name AS publisher,
format.name AS format,
genre.name AS genre
FROM 
book
JOIN author USING(author_id)
JOIN publisher USING(publisher_id)
JOIN format USING(format_id)
JOIN genre USING(genre_id);

UPDATE book_list
SET title='SUNNY ISLAND'
WHERE
title='Island';