describe books.catalog;
describe book;
select
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print
FROM
book;

DESCRIBE books.catalog;
USE bookstore;
DESCRIBE book;
SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at as modified_at
FROM
book,author,publisher,genre,format
WHERE 
book.author_id= author.author_id
AND 
book.publisher_id = publisher.publisher_id
AND
book.genre_id=genre.genre_id
AND 
book.format_id=format.format_id;



SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at as modified_at
FROM
book
JOIN author ON book.author_id=author.author_id
JOIN publisher ON book.publisher_id=publisher.publisher_id
JOIN format ON book.format_id=format.format_id
JOIN genre ON book.genre_id=genre.genre_id;



SELECT
book.book_id,
book.title,
book.year_published,
book.num_pages,
book.in_print,
author.name as author,
author.country as author_country,
publisher.name as publisher,
publisher.city as publisher_city,
publisher.phone as publisher_phone,
genre.name as genre,
book.price as price,
format.name as format,
book.modified_at as modified_at
FROM
book
JOIN author USING (author_id)
JOIN publisher USING (publisher_id)
JOIN format USING (format_id)
JOIN genre USING (genre_id);
