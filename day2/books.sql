DROP TABLE IF EXISTS `books`;

CREATE TABLE
books
(
id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
 title VARCHAR(255),
  author VARCHAR(255),
  num_pages INT,
  year_published INT,
  in_print BOOL
  
);